import React from "react";
import "./App.css"

class App extends React.Component {

    constructor() {
        super()
        this.state = {
            userName: '',
            userEmail: '',
            birthday: '',
            numValue: '',
            validation: {
                form: false,
                userName: null,
                userEmail: null,
                birthday: null,
                numValue: null,
            }
        }

        this.formRef = React.createRef()
        this.nameRef = React.createRef()
        this.emailRef = React.createRef()
        this.birthdayRef = React.createRef()
        this.numValueRef = React.createRef()
    }

    handleInput(event, fieldRef) {
        let fieldName = event.target.name

        let currentValidation = this.state.validation
        currentValidation.form = this.formRef.current.checkValidity()
        currentValidation[fieldName] = fieldRef.current.validity

        this.setState({
            [fieldName]: event.target.value,
            validation: currentValidation
        })
    }

    render() {
        return (
            <form ref={this.formRef} noValidate>
                <h1>Exemplo Forms</h1>

                Informe o nome: <br />
                <input type="text" name="userName" value={this.state.userName} ref={this.nameRef}
                    required onChange={event => this.handleInput(event, this.nameRef)} />

                {
                    this.state.validation.userName &&
                    !this.state.validation.userName.valid &&
                    <div className="error-message">Nome é obrigatório</div>
                }

                <br />

                Informe o e-mail: <br />
                <input type="email" name="userEmail" value={this.state.userEmail} ref={this.emailRef}
                    required onChange={event => this.handleInput(event, this.emailRef)} />

                {
                    this.state.validation.userEmail &&
                    this.state.validation.userEmail.valueMissing &&
                    <div className="error-message">E-mail é obrigatório</div>
                }

                {
                    this.state.validation.userEmail &&
                    !this.state.validation.userEmail.valueMissing &&
                    !this.state.validation.userEmail.valid &&
                    <div className="error-message">E-mail inválido</div>
                }

                <br />

                Informe a data de nascimento: <br />
                <input type="date" name="birthday" value={this.state.birthday} ref={this.birthdayRef}
                    required onChange={event => this.handleInput(event, this.birthdayRef)} />

                {
                    this.state.validation.birthday &&
                    !this.state.validation.birthday.valid &&
                    <div className="error-message">Data de nascimento é obrigatória</div>
                }

                <br />

                Informe um valor numérico: <br />
                <input type="number" name="numValue" value={this.state.numValue} ref={this.numValueRef}
                    required onChange={event => this.handleInput(event, this.numValueRef)} />

                {
                    this.state.validation.numValue && 
                    !this.state.validation.numValue.valid && 
                    <div className="error-message">Número é obrigatório</div>
                }

                <br />

                <input type="button" value="Enviar" disabled={!this.state.validation.form} />
            </form>
        )
    }

}

export default App
